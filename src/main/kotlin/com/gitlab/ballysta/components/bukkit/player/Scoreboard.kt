package com.gitlab.ballysta.components.bukkit.player

import com.gitlab.ballysta.architecture.*
import org.bukkit.Bukkit.getScoreboardManager
import org.bukkit.ChatColor
import org.bukkit.ChatColor.*
import org.bukkit.entity.Player
import org.bukkit.scoreboard.DisplaySlot.*

private val BLANKS = Array(16) { String(charArrayOf(COLOR_CHAR, 's' + it - 1)) }
val Player.sidebar get(): Toggled.(Int, Observable<String>) -> (Unit) = { index, line ->
    //Setup scoreboard on the player if applicable.
    if (scoreboard == getScoreboardManager()!!.mainScoreboard)
        scoreboard = getScoreboardManager()!!.newScoreboard.apply {
            registerNewObjective("sidebar", "dummy").displaySlot = SIDEBAR
            BLANKS.forEach { registerNewTeam(it).addEntry(it) }
        }
    val objective = scoreboard.getObjective(SIDEBAR)!!
    val score = objective.getScore(BLANKS[index])
    val team = scoreboard.getTeam(BLANKS[index])!!
    line.map { it.substring(0..minOf(it.lastIndex, 29)) }() { if (index != 0) {
        val split = if (it.length < 16) 0 else if (it[15] == '§') 15 else 16
        team.prefix = if (split == 0) it else it.substring(0, split)
        val color = if (split == 16) getLastColors(team.prefix) else ""
        team.suffix = if (split == 0) "" else color + it.substring(split)
    } else objective.displayName = it }
    onEnabled { if (!score.isScoreSet) score.score = 15 - index }
    onDisabled { scoreboard.resetScores(BLANKS[index]) }
}